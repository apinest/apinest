package nestMiddlewares

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

// Middleware tests
func TestMiddlewares(t *testing.T) {

	// Test header name
	testHeaderName := "testHeader"

	// First middleware for testing
	middleware1 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			req.Header.Set(testHeaderName, "1")
			next.ServeHTTP(res, req)
		})
	}

	// Second middleware for testing
	middleware2 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			testHeaderValue := req.Header.Get(testHeaderName)
			if testHeaderValue != "1" {
				t.Error("Test header value incorrect")
			} else {
				req.Header.Set(testHeaderName, "2")
				next.ServeHTTP(res, req)
			}
		})
	}

	// Third middleware for testing
	middleware3 := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			testHeaderValue := req.Header.Get(testHeaderName)
			if testHeaderValue != "2" {
				t.Error("Test header value incorrect")
			} else {
				req.Header.Set(testHeaderName, "3")
				next.ServeHTTP(res, req)
			}
		})
	}

	// Final handler func
	handlerFunc := func(res http.ResponseWriter, req *http.Request) {
		testHeaderValue := req.Header.Get(testHeaderName)
		if testHeaderValue != "3" {
			t.Error("Test header value incorrect")
		}
	}

	// Add the middlewares to middleware collection
	middlewareCollection := New()
	middlewareCollection.Add(middleware1)
	middlewareCollection.Add(middleware2)
	middlewareCollection.Add(middleware3)

	// create the middleware chain from the middleware collection
	middlewareChain := middlewareCollection.ThenFunc(handlerFunc)

	// create a mock request to use
	req := httptest.NewRequest("GET", "http://testing", nil)

	// call the handler using a mock response recorder (we'll not use that anyway)
	middlewareChain.ServeHTTP(httptest.NewRecorder(), req)

}
