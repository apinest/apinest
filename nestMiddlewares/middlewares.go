package nestMiddlewares

import (
	"net/http"
)

type Middleware func(http.Handler) http.Handler

// Collection of middlwares with custom methods
type Collection struct {
	middlewares []Middleware
}

// Constructor to create new Collection
func New(collections ...Collection) Collection {
	newCollection := Collection{}
	for _, collection := range collections {
		newCollection.middlewares = append(([]Middleware)(nil), collection.middlewares...)
	}
	return newCollection
}

// Add new middleware to Collection
func (c *Collection) Add(middleware Middleware) *Collection {
	c.middlewares = append(c.middlewares, middleware)
	return c
}

// Create chain call of the middlewares
func (c Collection) Then(h http.Handler) http.Handler {
	if h == nil {
		h = http.DefaultServeMux
	}

	for i := range c.middlewares {
		h = c.middlewares[len(c.middlewares)-1-i](h)
	}

	return h
}

// Specify the final handler function
func (c Collection) ThenFunc(fn http.HandlerFunc) http.Handler {
	if fn == nil {
		return c.Then(nil)
	}
	return c.Then(fn)
}
