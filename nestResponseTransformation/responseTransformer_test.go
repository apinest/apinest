package nestResponseTransformation

import (
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var responseTransformer ResponseTransformer

func TestAddHeader(t *testing.T) {
	res := httptest.NewRecorder()
	responseTransformer.Response = res

	responseTransformer.AddHeader("Test", "Test")

	assert.Equal(t, "Test", res.Header().Get("Test"))
}

func TestReplaceHeader(t *testing.T) {
	res := httptest.NewRecorder()
	responseTransformer.Response = res
	res.Header().Add("Content-Type", "value")

	responseTransformer.ReplaceHeader("Content-Type", "new-value")

	assert.Equal(t, "new-value", res.Header().Get("Content-Type"))
}

func TestRemoveHeader(t *testing.T) {
	res := httptest.NewRecorder()
	responseTransformer.Response = res
	res.Header().Add("Content-Type", "value")

	responseTransformer.RemoveHeader("Content-Type")

	assert.Equal(t, "", res.Header().Get("Content-Type"))
}

func TestAppendHeader(t *testing.T) {
	res := httptest.NewRecorder()
	responseTransformer.Response = res
	res.Header().Add("Test", "value")

	responseTransformer.AppendHeader("Test", "test")

	for name, value := range res.Header() {
		if name == "Test" {
			assert.Equal(t, "value test", strings.Join(value[:], " "))
		}
	}
}
