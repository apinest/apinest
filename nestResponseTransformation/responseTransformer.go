package nestResponseTransformation

import "net/http"

type ResponseTransformer struct {
	Response http.ResponseWriter
}

func (rt *ResponseTransformer) GetHeader(name string) string {
	return rt.Response.Header().Get(name)
}

// If and only if the header is not already set, set a new header with the given value. Ignored if the header is already set.
func (rt *ResponseTransformer) AddHeader(name, value string) {
	if rt.GetHeader(name) == "" {
		rt.Response.Header().Add(name, value)
	}
}

// If the header is not set, set it with the given value. If it is already set, the new-value is appended to the old-value separated by a space.
func (rt *ResponseTransformer) AppendHeader(name, value string) {
	rt.Response.Header().Add(name, value)
}

// Unset the headers with the given name.
func (rt *ResponseTransformer) RemoveHeader(name string) {
	rt.Response.Header().Del(name)
}

// If and only if the header is already set, replace its old value with the new one. Ignored if the header is not already set.
func (rt *ResponseTransformer) ReplaceHeader(name, value string) {
	if rt.GetHeader(name) != "" {
		rt.Response.Header().Set(name, value)
	}
}

// Rename a response header.
func (rt *ResponseTransformer) RenameHeader(oldName, newName string) {
	value := rt.GetHeader(oldName)
	if value != "" {
		rt.RemoveHeader(oldName)
		rt.AddHeader(newName, value)
	}
}
