package nestReverseProxy

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestReverseProxy(t *testing.T) {
	target := "http://google.com/"
	req, _ := http.NewRequest("GET", "/", nil)
	res := httptest.NewRecorder()
	serveReverseProxy(target, res, req)

	expectedStatusCode := 301
	if actualStatusCode := res.Code; actualStatusCode != expectedStatusCode {
		t.Errorf("actual status code = %d, expected status code = %d", actualStatusCode, expectedStatusCode)
	}
}
