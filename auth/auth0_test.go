package auth

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

// Test for Auth0
func TestAuth0(t *testing.T) {
	// Next handler function if the middleware executes without error
	handlerFunc := http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		t.Error("Auth0 middleware failed to block request with invalid header")
	})

	testSet := getAuth0TestSet()

	// Run each testcase
	for i, testCase := range testSet {
		fmt.Println("________________________\nTest case: ", i, "\n-------------------------")
		// create a mock request to use
		req := httptest.NewRequest("GET", "http://testing", nil)
		// create mock response writer
		res := httptest.NewRecorder()
		// Add authorization header
		req.Header.Set("Authorization", testCase.headerValue)

		// Create auth0 middleware
		auth0Middleware := Auth0(testCase.authParams, testCase.strict)

		// Call the middleware passing next handlerFunc
		handlerToTest := auth0Middleware(handlerFunc)

		handlerToTest.ServeHTTP(res, req)
		result := res.Result()

		// validate response status code
		if result.StatusCode != testCase.expectedStatusCode {
			t.Error("Expected status code: " + strconv.Itoa(testCase.expectedStatusCode) + ", got: " + strconv.Itoa(result.StatusCode))
		}

		body := res.Body.String()
		// validate response body
		if body != testCase.expectedBody {
			t.Error("Expected body: \"" + testCase.expectedBody + "\", got: \"" + body + "\"")
		}
	}
}

// Test Case struct
type Auth0TestCase struct {
	strict             bool
	headerValue        string
	expectedStatusCode int
	expectedBody       string
	authParams         Auth0Params
}

// Create different testcases
func getAuth0TestSet() []Auth0TestCase {
	const domain = "https://dev-apinest.us.auth0.com/"
	authParams := Auth0Params{
		Auth0ApiAudience: []string{fmt.Sprintf("%sapi/v2/", domain), fmt.Sprintf("%suserinfo", domain)},
		Auth0Domain:      domain,
	}

	testSet := []Auth0TestCase{
		{false, "sample", 401, "Invalid Authorization Header", authParams},
		{false, "Bearer sample", 401, "Invalid IdToken", authParams},
		{true, "", 401, "Unauthenticated", authParams},
		// {true, fmt.Sprintf("Bearer %v", token), 0, "", authParams},
	}
	return testSet
}
